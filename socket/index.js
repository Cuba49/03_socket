import { texts } from "../data";
import * as config from "./config";
let users = [];
let rooms = [];
let roomMap = new Map(rooms.map((room) => [room.Name, room]));
const roomsForDisplay = () =>
  rooms.filter(
    (room) => room.Users.length <= config.MAXIMUM_USERS_FOR_ONE_ROOM&&!room.isGame
  );
const getCurrentRoomId = (socket) =>
  Object.keys(socket.rooms).find((room) => roomMap.has(room));

export default (io) => {
  io.on("connection", (socket) => {
    let currentRoomId = null;
    let randomText;
    let awaitChar;
    let timer;
    socket.emit("UPDATE_ROOMS", roomsForDisplay());
    const username = socket.handshake.query.username;
    if (users.indexOf(username) !== -1) {
      socket.emit("EXIST_USER");
      return;
    }
    if (username !== "null") {
      users.push(username);
    }
    const joinRoom = (roomId) => {
      const prevRoomId = getCurrentRoomId(socket);
      if (prevRoomId) {
        socket.leave(prevRoomId);
      }
      const room = roomMap.get(roomId);
      if (room.Users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.join(roomId, () => {
          room.Users.push({ Name: username, IsReady: false, Progress: 0, Result:0 });
          io.to(socket.id).emit("JOIN_ROOM_DONE", { roomId, room });
          io.to(roomId).emit("UPDATE_ROOM", room);
          currentRoomId = roomId;
          io.emit("UPDATE_ROOMS", roomsForDisplay());
        });
      }
    };
    const leaveRoom = (roomId) => {
      if (roomId) {
        socket.leave(roomId);
        const room = roomMap.get(roomId);
        room.Users = room.Users.filter((user) => user.Name !== username);
        if (room.Users.length === 0) {
          rooms.splice(rooms.indexOf(room), 1);
          roomMap = new Map(rooms.map((room) => [room.Name, room]));
        } else {
          io.to(roomId).emit("UPDATE_ROOM", room);
          if (room.Users.every((user) => user.IsReady)&&!room.isGame) {
            readyToStart(roomId);
          }else if (room.Users.every((user) => user.Progress===100)&&room.isGame) {
            endGame(roomId);
          }
        }
      }
      io.emit("UPDATE_ROOMS", roomsForDisplay());
    };
    const random = (max) => {
      max = Math.floor(max);
      return Math.floor(Math.random() * (max + 1));
    };
    const endGame = (roomId) => {
      const room = roomMap.get(roomId);
      if (room.isGame) {
        io.to(roomId).emit(
          "GAME_OVER",
          room.Users.sort((a, b) => b.Result - a.Result).map(user => { return user.Name })
        );
        room.Users = room.Users.map(user => { user.IsReady = false; user.Progress = 0; return user});
        room.isGame = false;
        io.to(roomId).emit("UPDATE_ROOM", room);
        io.emit("UPDATE_ROOMS", roomsForDisplay());
      }
    }
    const readyToStart = (roomId) => {
      const randomIndex = random(texts.length-1);
      const room = roomMap.get(roomId);
      room.isGame = true;
      io.to(roomId).emit(
        "READY_TO_START",
        config.SECONDS_TIMER_BEFORE_START_GAME,
        randomIndex
      );
      setTimeout(() => {
        io.to(roomId).emit("START", config.SECONDS_FOR_GAME);
        setTimeout(() => { endGame(roomId); }, config.SECONDS_FOR_GAME * 1000);
      }, config.SECONDS_TIMER_BEFORE_START_GAME * 1000);
      io.emit("UPDATE_ROOMS", roomsForDisplay());
    };
    const isCorrectChar = (char, seconds) => {
      if (randomText[awaitChar] === char) {
        awaitChar++;
        socket.emit("NEXT_CHAR", awaitChar);
        const prevRoomId = getCurrentRoomId(socket);
        const room = roomMap.get(prevRoomId);
        room.Users = room.Users.map((user) => {
          if (user.Name === username) {
            user.Progress = (awaitChar * 100) / randomText.length;
            if (user.Progress === 100) {
              user.Result = seconds;
            } else {
              user.Result = user.Progress-100;
            }
          }
          return user;
        });
        if(room.Users.every(user=>user.Progress===100))endGame(prevRoomId);
        io.to(prevRoomId).emit("UPDATE_ROOM", room);
      }
    };
    const prepareText = (indexText) => {
      randomText = texts[indexText].split("");
      awaitChar = 0;
    };
    socket.on("PREPARE_TEXT", prepareText);
    socket.on("NEW_CHAR", isCorrectChar);
    socket.on("JOIN_ROOM", joinRoom);
    socket.on("LEAVE_ROOM", leaveRoom);
    socket.on("CREATE_ROOM", (name) => {
      if (!roomMap.has(name)) {
        rooms.push({ Name: name, Users: [], isGame:false });
        roomMap = new Map(rooms.map((room) => [room.Name, room]));
        joinRoom(name);
      } else {
        socket.emit("EXIST_ROOM");
      }
    });
    socket.on("READY", () => {
      awaitChar = 0;
      const prevRoomId = getCurrentRoomId(socket);
      const room = roomMap.get(prevRoomId);
      let ready;
      room.Users = room.Users.map((user) => {
        if (user.Name === username) {
          user.IsReady = !user.IsReady;
          ready = user.IsReady;
        }
        return user;
      });
      socket.emit("READY_BUTTON", ready);
      io.to(prevRoomId).emit("UPDATE_ROOM", room);
      if (room.Users.every((user) => user.IsReady)) {
        readyToStart(prevRoomId);
      }
    });
    socket.on("disconnect", () => {
      users = users.filter((user) => user !== username);
      leaveRoom(currentRoomId);
    });
  });
};
