import {
  createElement,
  addClass,
  removeClass,
  createRoom,
  createUser,
  createChar,
} from "./helper.mjs";

const roomsList = document.getElementById("room-list");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const roomName = document.getElementById("room-name");
const listUsers = document.getElementById("list-users");
const btnBack = document.getElementById("quit-room-btn");
const btnCreateRoom = document.getElementById("add-room-btn");
const btnReady = document.getElementById("ready-btn");
const readyToGo = document.getElementById("timer");
const randomTextBlock = document.getElementById("text-container");
const secondsLeft = document.getElementById("seconds-left");
const listLinners = document.getElementById("list-winners");
const result = document.getElementById("result");
const btnResults=  document.getElementById("quit-results-btn");

const username = sessionStorage.getItem("username");
let activeRoomId = null;
let randomText = null;

const socket = io("", { query: { username } });
if (!username) {
  window.location.replace("/login");
}

const setActiveRoomId = (roomId) => {
  activeRoomId = roomId;
};

const onCreateRoom = () => {
  const result = window.prompt("Enter name room");
  if (result) socket.emit("CREATE_ROOM", result);
};

const onBackFromRoom = () => {
  addClass(gamePage, "display-none");
  removeClass(roomsPage, "display-none");
  if (activeRoomId) socket.emit("LEAVE_ROOM", activeRoomId);
};

const existUser = () => {
  alert("User is alredy exist");
  sessionStorage.clear();
  window.location.replace("/login");
};

const existRoom = () => {
  alert("Room is alredy exist");
};

const updateRooms = (rooms) => {
  const allRooms = rooms.map((room) => createRoom(room, socket));
  roomsList.innerHTML = "";
  roomsList.append(...allRooms);
};

const updateRoom = (room) => {
  roomName.innerText = room.Name;
  const allUsers = room.Users.map((user) => createUser(user, username));
  listUsers.innerHTML = "";
  listUsers.append(...allUsers);
};

const joinRoomDone = ({ roomId, room }) => {
  addClass(roomsPage, "display-none");
  removeClass(gamePage, "display-none");
  removeClass(btnReady, "display-none");
  btnReady.innerText = "Ready";
  addClass(readyToGo, "display-none");
  addClass(randomTextBlock, "display-none");
  addClass(secondsLeft, "display-none");
  setActiveRoomId(roomId);
  updateRoom(room);
};

const readyToStart = (seconds, indexText) => {
  randomText = null;
  fetch(`/game/texts/${indexText}`)
    .then((response) => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json();
    })
    .then((data) => {
      randomText = data.Text.split("");
    });
  addClass(btnBack, "display-none");
  addClass(btnReady, "display-none");
  readyToGo.innerText = seconds;
  socket.emit("PREPARE_TEXT", indexText);
  removeClass(readyToGo, "display-none");
  const interval = setInterval(() => {
    if (seconds > 0) {
      seconds--;
      readyToGo.innerText = seconds;
    } else {
      clearInterval(interval);
    }
  }, 1000);
};
let secondsGame;
const start = (seconds) => {
  
  setNewViewText(0);
  addClass(readyToGo, "display-none");
  removeClass(randomTextBlock, "display-none");
  removeClass(secondsLeft, "display-none");
  secondsLeft.innerText = seconds+" seconds left";
  const interval = setInterval(() => {
    if (seconds > 0) {
      seconds--;
      secondsGame = seconds;
      secondsLeft.innerText = seconds+" seconds left";
    } else {
      clearInterval(interval);
    }
  }, 1000);
  document.addEventListener("keydown", listenerKey);
};

const listenerKey = (ev) => { socket.emit("NEW_CHAR", ev.key, secondsGame) };

const setNewViewText=(awaitChar) => {
  const allChars = randomText.map((char, index) => createChar(char, (index<awaitChar?"char-confirm":(index===awaitChar?"char-next":""))));
  randomTextBlock.innerHTML = "";
  randomTextBlock.append(...allChars);
}

const gameOver = (users) => {
  listLinners.innerHTML = "";
  const allWinners = users.map((user, index) => {
    const userLine = createElement({ tagName: "p",className:"", attributes: { id: `place-${index+1}` } });
    userLine.innerHTML = index+1 + ". " + user
    return userLine;
  })
  listLinners.append(...allWinners);
  removeClass(result, "display-none");

  addClass(secondsLeft, "display-none");
  document.removeEventListener("keydown",listenerKey);
}
  
const closeModal = () => {
  addClass(randomTextBlock, "display-none");
  btnReady.innerText = "Ready";
  removeClass(btnReady, "display-none");
  removeClass(btnBack, "display-none");
  addClass(result, "display-none");
}

socket.on("EXIST_USER", existUser);
socket.on("EXIST_ROOM", existRoom);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("UPDATE_ROOM", updateRoom);
socket.on(
  "READY_BUTTON",
  (butt) => (btnReady.innerText = butt ? "Not ready" : "Ready")
);
socket.on("READY_TO_START", readyToStart);
socket.on("START", start);
socket.on("NEXT_CHAR", setNewViewText);
socket.on("GAME_OVER", gameOver);
btnBack.addEventListener("click", onBackFromRoom);
btnCreateRoom.addEventListener("click", onCreateRoom);
btnReady.addEventListener("click", () => socket.emit("READY"));
btnResults.addEventListener("click", closeModal);
onBackFromRoom();
