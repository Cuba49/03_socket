export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);

export const createChar = (char, style) => {
  const point = createElement({
    tagName: "p",
    className: "char "+style,
  });
  point.innerText = char;
  return point;
};

export const createRoom = (roomObj, socket) => {
  const room = createElement({
    tagName: "div",
    className: "room",
  });

  const connectedUsers = createElement({
    tagName: "p",
    className: "connected-users",
  });
  connectedUsers.innerText = `${roomObj.Users.length} users connected`;
  room.append(connectedUsers);

  const name = createElement({
    tagName: "p",
    className: "room-name",
  });
  name.innerText = `${roomObj.Name}`;
  room.append(name);

  const roomButton = createElement({
    tagName: "button",
    className: "join-btn",
  });
  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", roomObj.Name);
  };
  roomButton.addEventListener("click", onJoinRoom);
  roomButton.innerText = "Join";
  room.append(roomButton);

  return room;
};
export const createUser = (user, username) => {
  const userBlock = createElement({
    tagName: "div",
    className: "user",
  });
  const userTop = createElement({
    tagName: "div",
    className: "flex user-top"
  });
  const ready = createElement({
    tagName: "div",
    className: "ready-status "+(user.IsReady?"ready-status-green":"ready-status-red")
  });
  const nameUser = createElement({
    tagName: "p",
  });
  nameUser.innerText = `${user.Name}` + (user.Name === username ? " (you)" : "");
  userTop.append(ready);
  userTop.append(nameUser);
  userBlock.append(userTop);
  const progressContainer = createElement({
    tagName: "div",
    className: "progress-container",
  });
  const progress = createElement({
    tagName: "div",
    className: `user-progress ${user.Name}`+(user.Progress===100?" finish":""),
    attributes: { style:`width:${user.Progress}%` }
  });
  progressContainer.append(progress);
  userBlock.append(progressContainer);
  return userBlock;
}
